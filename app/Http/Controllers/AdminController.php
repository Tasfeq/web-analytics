<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
use Auth;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

    public function showLogin(){

        return view('Login.login');
    }

    public function emailLogin(Request $request){

        $userData=array('email'=>$request->input('email'),
            'password'=>$request->input('password'));
        $rules=array('email'=>'required',
            'password'=>'required');
        $validator=  Validator::make($userData,$rules);
        if($validator->fails())
        {
            return Redirect::back()->withErrors($validator);
        }
        else{
            if(Auth::attempt($userData))
            {
                $user=Auth::user();
                Session::put('name',$user->name);
                Session::put('user_id',$user->id);
                return Redirect::to('dashboard-panel');

            }
            else{
                return Redirect::back()->withErrors($validator);
            }
        }
    }

    public function logout(){

        Auth::logout();
        return Redirect::to('/login');
    }
}
