<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Video;
use App\Model\Person;
use App\Model\ProductionHouse;
use App\Model\VideoWatchLog;
use Illuminate\Support\Facades\DB;

use Auth;

class DashboardController extends Controller
{
    private $model;

    public function __construct(Video $model)
    {
        $this->model = $model;
        if(!Auth::check())
        {
            return view('login.login');
        }
    }


    public function showDashboard(){

        if(!Auth::check())
        {
            return view('login.login');
        }
        else{
            $dashboardData=$this->getNumbersForDashboardCards();
            $pieData=$this->getPieChartData();
            $browserData=$this->getBrowserData();
            $browserDataInPercent=$this->makeBrowserDataInPercent($browserData);
            return view('Dashboard._layout.main_content',compact('dashboardData','pieData','browserDataInPercent'));
        }
    }

    public function getNumbersForDashboardCards(){

        $totalVideo=Video::all()->count();
        $totalStars=Person::all()->count();
        $totalHouses=ProductionHouse::all()->count();
        $totalHours=VideoWatchLog::all()->sum('time');
        $Data=array('videoTotal'=>$totalVideo,'starTotal'=>$totalStars,'houseTotal'=>$totalHouses,
                    'totalHours'=>gmdate("H:i:s", $totalHours));
        return $Data;
    }

    public function getPieChartData(){

        $musicVideos=$this->model->with('categories')
                     ->whereHas('categories',function($q) {
                     $q->where('categories.name','=','Music');
                     })->get()->count();
        $dramaVideos=$this->model->with('categories')
            ->whereHas('categories',function($q) {
                $q->where('categories.name','=','Drama');
            })->get()->count();
        $lifestyleVideos=$this->model->with('categories')
            ->whereHas('categories',function($q) {
                $q->where('categories.name','=','LifeStyle');
            })->get()->count();
        $movieVideos=$this->model->with('categories')
            ->whereHas('categories',function($q) {
                $q->where('categories.name','=','Movie');
            })->get()->count();

        $pieData=array('musicVideos'=>$musicVideos,'dramaVideos'=>$dramaVideos,'lifestyleVideos'=>$lifestyleVideos,
                       'movieVideos'=>$movieVideos);

        return $pieData;
    }

    public function getBrowserData(){

        $operaData=VideoWatchLog::where('browser','=','opera')->get()->count();
        $chromeData=VideoWatchLog::where('browser','=','chrome')->get()->count();
        $safariData=VideoWatchLog::where('browser','=','safari')->get()->count();
        $explorerData=VideoWatchLog::where('browser','=','ie')->get()->count();
        $mozillaData=VideoWatchLog::where('browser','=','mozilla')->get()->count();

        $browserData=array('operaData'=>$operaData,'chromeData'=>$chromeData,'safariData'=>$safariData,
                           'explorerData'=>$explorerData,'mozillaData'=>$mozillaData);

        return $browserData;
    }

    public function makeBrowserDataInPercent($browserData){

        $totalBrowserSum = $browserData['operaData']+$browserData['chromeData']+$browserData['safariData']+
            $browserData['explorerData']+$browserData['mozillaData'];

        $opera = round(($browserData['operaData'] / $totalBrowserSum) * 100);
        $chrome = round(($browserData['chromeData'] / $totalBrowserSum) * 100);
        $safari = round(($browserData['safariData'] / $totalBrowserSum) * 100);
        $explorer = round(($browserData['explorerData'] / $totalBrowserSum) * 100);
        $mozilla = round(($browserData['mozillaData'] / $totalBrowserSum) * 100);

        $percentData=array('opera'=>$opera,'chrome'=>$chrome,'safari'=>$safari,'explorer'=>$explorer,'mozilla'=>$mozilla);
        return $percentData;
    }

    public function showUsers(){

        $users=\App\User::all();
        return view('Dashboard._layout.user_statistics',compact('users'));
    }


    public function userSearchResult(Request $request){

        $userId=$request->input('user-id');

        $userData=VideoWatchLog::where('user_id','=',$userId)->get();
        $totalHours=VideoWatchLog::where('user_id','=',$userId)->sum('time');
        $platformData=$this->userPlatformData($userId);
        $browserData=$this->getUserBrowserData($userId);
        $browserDataInPercent=$this->makeBrowserDataInPercent($browserData);

        return response()->json(['data' => $userData,'browserDataInPercent'=>$browserDataInPercent,
                                'totalHours'=>gmdate("H:i:s", $totalHours),'platformInfo'=>$platformData]);
    }

    public function getUserBrowserData($userId){

        $operaData=VideoWatchLog::where('browser','=','opera')->where('user_id','=',$userId)->get()->count();
        $chromeData=VideoWatchLog::where('browser','=','chrome')->where('user_id','=',$userId)->get()->count();
        $safariData=VideoWatchLog::where('browser','=','safari')->where('user_id','=',$userId)->get()->count();
        $explorerData=VideoWatchLog::where('browser','=','ie')->where('user_id','=',$userId)->get()->count();
        $mozillaData=VideoWatchLog::where('browser','=','mozilla')->where('user_id','=',$userId)->get()->count();

        $browserData=array('operaData'=>$operaData,'chromeData'=>$chromeData,'safariData'=>$safariData,
            'explorerData'=>$explorerData,'mozillaData'=>$mozillaData);

        return $browserData;
    }

    public function userPlatformData($userId){

        $mobileView=VideoWatchLog::where('platform','=','app')->where('user_id','=',$userId)->get()->count();
        $desktopView=VideoWatchLog::where('platform','=','web')->where('user_id','=',$userId)->get()->count();
        if($mobileView>$desktopView){
            $result="mobile";
        }else{
            $result="desktop";
        }
        return $data=array('mobile'=>$mobileView,'desktop'=>$desktopView,'res'=>$result);
    }

    public function showVideos(){

        $videos=Video::with(['productionHouse'])->get()->toArray();
        $mostPlayedVideo=$this->getMostPlayedVideo();
        return view('Dashboard._layout.video_statistics',compact('videos','mostPlayedVideo'));
    }

    public function videoSearchResult(Request $request){

        $videoId=$request->input('video-id');

        $totalView=VideoWatchLog::where('video_id','=',$videoId)->get()->count();
        $viewDate = DB::table('video_watch_logs')
            ->select(DB::raw('count(*) as view_count, created_at'))
            ->where('video_id', '=', $videoId)
            ->groupBy('created_at')
            ->get();

        return response()->json(['viewDate' => $viewDate,'totalView'=>$totalView]);
    }

    public function getMostPlayedVideo(){

        $video=DB::table('videos')
            ->leftJoin(DB::raw('(SELECT `video_watch_logs`.`video_id`,COUNT(*) AS top
                                    FROM `video_watch_logs`
                                    GROUP BY video_watch_logs.`video_id` ORDER BY top DESC) as popular'),'popular.video_id','=','videos.id')
            ->orderBy('top','desc')
            ->take(1)
            ->get();

        return $video;

    }

    public function getLiveUserFeed(){

        $liveData= DB::table('video_watch_logs')
            ->select(DB::raw('count(*) as user_count, updated_at'))
            ->groupBy('updated_at')
            //->take(5)
            ->get();
        return response()->json(['liveData' => $liveData]);
    }

    public function getLiveUserFeedLatest(){


        $latestData=DB::table('video_watch_logs')
            ->select(DB::raw('count(*) as user_count, updated_at'))
            ->groupBy('updated_at')
            ->orderBy('id','desc')
            ->first();

        //$temp=$latestData->updated_at;
        //dd($latestData->updated_at);
        return response()->json(['latestData' => $latestData]);
    }
}
