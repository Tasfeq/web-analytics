<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/dashboard-panel', 'DashboardController@showDashboard');

//login route
Route::get('/',function(){
    return view('welcome');
});
Route::get('/login', 'AdminController@showLogin');
Route::get('/logout', 'AdminController@logout');
Route::post('/post-login', array('as' => 'user.login', 'uses' => 'AdminController@emailLogin'));



//User statistics route
Route::get('/user-statistics', 'DashboardController@showUsers');
Route::get('/search-result',array('as'=>'search.result','uses'=>'DashboardController@userSearchResult'));

//Video Statistics route
Route::get('/video-statistics', 'DashboardController@showVideos');
Route::get('/video-search-result',array('as'=>'video.search.result','uses'=>'DashboardController@videoSearchResult'));

//Live user feed route
Route::get('/live-user-feed',array('as'=>'live.user.feed','uses'=>'DashboardController@getLiveUserFeed'));
Route::get('/live-user-feed-latest',array('as'=>'live.user.feed.latest','uses'=>'DashboardController@getLiveUserFeedLatest'));

//Admin register route
Route::get('user-info',function(){
    $user=new App\User();
    $user->name='3rdbell_Admin';
    $user->email='3rdbell@gmail.com';
    $user->password=Illuminate\Support\Facades\Hash::make('n$Q%GwMsKpSIx$M1foXs^zLJ');
    $user->save();

});