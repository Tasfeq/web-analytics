<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table='videos';

    public function persons(){
        return $this->belongsToMany('App\Model\Person',
            'person_video', 'video_id', 'person_id')->withPivot('role');
    }
    
    public function productionHouse(){
        return $this->belongsTo('App\Model\ProductionHouse','production_house_id','id');
    }

    public function categories(){
        return $this->belongsToMany('App\Model\Category',
            'category_video', 'video_id', 'category_id');
    }
    public function userPlaylists(){
        return $this->hasMany('App\Model\UserPlaylist','video_id','id')
            ->selectRaw('count(*) as favourite')
            ->where('favourite','=',1)
            ->groupBy('video_id');
    }

    public function users(){
        return $this->belongsToMany('App\User','user_playlists','video_id','user_id')
                    ->withPivot('favourite','count','watched');
    }

    public function generalPlaylists(){
        return $this->hasMany('App\Model\GeneralPlaylist','video_id')
                    ->selectRaw('video_id, count(*) as view')
                    ->groupBy('video_id');
    }
    
    public function trendingVideo(){
        return $this->hasOne('App\Model\TrendingVideo','video_id');
    }

    public function recommendedVideo(){
        return $this->hasOne('App\Model\RecommendedVideo','video_id');
    }

}

