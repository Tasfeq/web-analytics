<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductionHouse extends Model
{
    protected $table='production_houses';

    public function videos(){

        return $this->hasMany('App\Model\Video');
    }
}

