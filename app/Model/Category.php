<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table='categories';
    protected $primaryKey = 'category_id';

    public function parent()
    {
        return $this->belongsTo('App\Model\Category','parent_id');
    }
    public function subCategory()
    {
        return $this->hasMany('App\Model\Category','parent_id');
    }
}

