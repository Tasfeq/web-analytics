<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPlaylist extends Model
{
    protected $table = 'user_playlists';
    
    protected $fillable = ['video_id','user_id','count','watched'];
}
