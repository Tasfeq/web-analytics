<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table= 'persons';
//    protected $primaryKey = 'post_id';

    public function videos(){
        return $this->belongsToMany('App\Model\Video',
            'person_video', 'person_id', 'video_id')->withPivot('role')->orderBy('person_video.role','desc');

    }
}
