<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Login Form</title>

    {!! HTML::style('assets/css/styleLogin.css') !!}

</head>

<body>

<div class="main-wrap">

    {!! Form::open(array('route' => 'user.login', 'class' => 'form-horizontal','files'=>true,'enctype' => 'multipart/form-data','method'=>'POST')) !!}

    <div class="login-main">
        <input type="text" placeholder="Email" class="box1 border1" name="email" required>
        <input type="password" placeholder="Password" class="box1 border2" name="password" required>
        <input type="submit" class="send" value="Login">

        {{--<p>Forgot Your Password? <a href="#">click here</a></p>--}}
    </div>

    {!! Form::close() !!}

</div>

{!! HTML::script('assets/js/jquery-1.10.2.js') !!}

<script>

    $(document).ready(function(){

        function showWelcome()
        {
            console.log('hit');
        }
    })
</script>


</body>
</html>
