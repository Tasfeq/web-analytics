@extends('Dashboard._layout.master')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="icon-big icon-warning text-center">
                                        <i class="ti-video-camera"></i>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="numbers">
                                        <p>Videos</p>
                                        {{ $dashboardData['videoTotal'] }}
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <i class="ti-reload"></i> Updated now
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="icon-big icon-success text-center">
                                        <i class="ti-user"></i>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="numbers">
                                        <p>Profile</p>
                                        {{ $dashboardData['starTotal'] }}
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <i class="ti-reload"></i> Updated now
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="icon-big icon-danger text-center">
                                        <i class="ti-home"></i>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="numbers">
                                        <p>Pro.Houses</p>
                                        {{ $dashboardData['houseTotal'] }}
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <i class="ti-reload"></i> Updated now
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="icon-big icon-info text-center">
                                        <i class="ti-time"></i>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="numbers">
                                        <p>Views (Hrs)</p>
                                        {{ $dashboardData['totalHours'] }}
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <hr />
                                <div class="stats">
                                    <i class="ti-reload"></i> Updated now
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Active User Live Feed</h4>
                        </div>
                        <div class="content">
                            <div id="contenitore">
                                <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Browser Version Chart</h4>
                            <p class="category">Percentage of Used Browser</p>
                        </div>
                        <div class="content">
                            <div id="contenitore">
                                <div class="left">
                                    <table>
                                        <caption>Browser List (%)</caption>
                                        <tbody>
                                        <tr><td>Chrome</td><td>{{$browserDataInPercent['chrome']}}</td><td style="background-color:#336699">&nbsp;</td></tr>
                                        <tr><td>Opera</td><td>{{$browserDataInPercent['opera']}}</td><td style="background-color:#003366">&nbsp;</td></tr>
                                        <tr><td>IE</td><td>{{$browserDataInPercent['explorer']}}</td><td style="background-color:#ff6600">&nbsp;</td></tr>
                                        <tr><td>Mozilla</td><td>{{$browserDataInPercent['mozilla']}}</td><td style="background-color:#ffcc00">&nbsp;</td></tr>
                                        <tr><td>Safari</td><td>{{$browserDataInPercent['safari']}}</td><td style="background-color:#7CFC00">&nbsp;</td></tr>
                                        </tbody></table>
                                    <div class="button" onclick="viewGraph()">See Graph</div>
                                </div>
                                <div class="left graphics" style="display:none;">
                                    <div id="grafico">
                                        <div class="riga" style="top:100%"><div></div></div>
                                        <div class="riga" style="top:75%"><div>25%</div></div>
                                        <div class="riga" style="top:50%"><div>50%</div></div>
                                        <div class="riga" style="top:25%"><div>75%</div></div>
                                        <div class="riga" style="top:0%"><div>100%</div></div>
                                        <div id="col0" style="left:0; background-color:#336699;" class="column"></div>
                                        <div id="col1" style="left:20%; background-color:#003366;" class="column"></div>
                                        <div id="col2" style="left:40%; background-color:#ff6600;" class="column"></div>
                                        <div id="col3" style="left:60%; background-color:#ffcc00;" class="column"></div>
                                        <div id="col4" style="left:80%; background-color:#7CFC00;" class="column"></div>
                                        <div id="col5" style="left:100%; background-color:#7CFC00;" class="column"></div>
                                    </div>
                                </div>
                                <div class="canc"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Video Chart</h4>
                            <p class="category">Videos In Percentage On The Basis of Category</p>
                        </div>
                        <div class="content">
                            <div class="">
                                <canvas id="canvas" width="400" height="300">
                                </canvas>
                            </div>

                            <div class="footer">
                                <div class="chart-legend">
                                    <i class="fa fa-circle text-success"></i> Drama
                                    <i class="fa fa-circle text-danger"></i> Music
                                    <i class="fa fa-circle text-warning"></i> Movie
                                    <i class="fa fa-circle text-info"></i> LifeStyle
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('Dashboard._layout.scripts')
@stop
