<!doctype html>
<html lang="en">
<head>

    @include('Dashboard._layout.links')

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

        <!--
            Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
            Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
        -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="{{URL::to('/dashboard-panel')}}" class="simple-text">
                    3rdbell
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="{{URL::to('/dashboard-panel')}}">
                        <i class="ti-panel"></i>
                        <p>3rdbell Analytics</p>
                    </a>
                </li>
            </ul>
            <ul class="nav">
                <li class="active">
                    <a href="{{URL::to('/user-statistics')}}">
                        <i class="ti-user"></i>
                        <p>User Statistics</p>
                    </a>
                </li>
            </ul>
            <ul class="nav">
                <li class="active">
                    <a href="{{URL::to('/video-statistics')}}">
                        <i class="ti-video-clapper"></i>
                        <p>Video Statistics</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{URL::to('/dashboard-panel')}}">Analytics Panel</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="{{URL::to('/logout')}}">
                                <i class="ti-settings"></i>
                                <p>SignOut</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        {{-- main content div --}}
        @yield('content')


        {{-- footer --}}
        @include('Dashboard._layout.footer')
    </div>
</div>


</body>

        @yield('page-specific-js')

</html>
