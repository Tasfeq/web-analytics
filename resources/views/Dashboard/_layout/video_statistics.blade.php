@extends('Dashboard._layout.master')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="header">
                            <h5 class="title">Video Statistics</h5>
                        </div>
                        <div class="content">

                            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Duration</th>
                                    <th>Production House</th>
                                    <th>Link</th>
                                    <th>Created at</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($videos as $key=>$video)
                                    <tr class="video-data" data-id="{{$video['id']}}" style="cursor: pointer">
                                        <td>{{$video['title']}}</td>
                                        <td>{{$video['duration']}}</td>
                                        <td>{{$video['production_house']['name']}}</td>
                                        <td><a href="{{$video['link']}}" target="_blank">{{$video['link']}}</a></td>
                                        <td>{{$video['created_at']}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="header">
                            <h6 class="top-video title">Most Watched Video</h6>
                            <label><h4 class="top-video">{{$mostPlayedVideo[0]->title}}</h4></label>
                            <img class="img-rounded" src="{{$mostPlayedVideo[0]->thumbnail}}" width="200px" height="150px">
                        </div>
                        <div class="content">

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Video Specific Chart</h4>
                            <h4 id="video-name"></h4>
                        </div>
                        <div class="content">
                            <div id="contenitore" tabindex='1' style="display: none">
                                <div id="chartContainer" style="height: 300px; width: 80%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Total Viewed</h4>
                        </div>
                        <div class="content timer" style="display:none">
                            <div class="icon-big icon-info text-center">
                                <i class="ti-eye"></i> <span class="Count" id="time"> 300</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop


@section('page-specific-js')

    {!! HTML::script('assets/js/jquery-1.10.2.js') !!}
    {!! HTML::script('assets/js/jquery.canvasjs.min.js') !!}
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            $('#example').dataTable({
                "order": [],
                "oLanguage": {
                    "sInfo": "Showing _START_ to _END_ of _TOTAL_ Videos",
                    "sInfoEmpty": "Showing 0 to 0 of 0 Videos",
                    "sEmptyTable": "No information available"
                }
            });

            $('#example').on('click','.video-data',function(){

                videoId=$(this).data('id');
                title=$(this).find("td:first").text();

                var url = '{{ URL::route('video.search.result') }}';
                $.ajax({
                    url: url,
                    data: { 'video-id' :videoId } ,
                    type: "GET",
                    success: function(response) {
                        setData(response,title);
                        showGraph(response.viewDate);
                    },
                    error: function(xhr, textStatus, thrownError) {
                        alert('Something went to wrong.Please Try again later...');
                    }
                });
            })

            function setData(response,title){

                console.log(response);
                $('#contenitore').show();
                var formhtml = '';
                formhtml = '<h4>'+title+'</h4>';
                document.getElementById('video-name').innerHTML = title;
                $('#contenitore').focus();

                showTime(response.totalView);

            }

            function showTime(totalView){

                document.getElementById('time').innerHTML =totalView;
                $('.content .timer').show();
                $('.Count').each(function () {
                    var $this = $(this);
                    jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function () {
                            $this.text(Math.ceil(this.Counter));
                        }
                    });
                });
            }

            function showGraph(viewDate){

                //console.log(viewDate);
                $(function () {

                    var data = [];
                    var dataSeries = { type: "area" };
                    var dataPoints = [];

                    $.each(viewDate,function(key,value){
                        //console.log(value.view_count);
                        dataPoints.push({
                           x: new Date(value.created_at),
                           y: value.view_count
                        });
                    });

                    dataSeries.dataPoints = dataPoints;
                    data.push(dataSeries);

                    var options = {
                        zoomEnabled: true,
                        animationEnabled: true,
                        title: {
                            text: "Video Watch Flow"
                        },
                        axisX: {
                            labelAngle: 30
                        },
                        axisY: {
                            includeZero: false
                        },
                        data: data
                    };

                    $("#chartContainer").CanvasJSChart(options);

                });
            }


        })
    </script>

@stop