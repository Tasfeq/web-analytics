<!--   Core JS Files   -->
{!! HTML::script('assets/js/jquery-1.10.2.js') !!}
{!! HTML::script('assets/js/bootstrap.min.js') !!}

        <!--  Checkbox, Radio & Switch Plugins -->
{!! HTML::script('assets/js/bootstrap-checkbox-radio.js') !!}

        <!--  Charts Plugin -->
{!! HTML::script('assets/js/chartist.min.js') !!}

        <!--  Notifications Plugin    -->
<script src=""></script>
{!! HTML::script('assets/js/bootstrap-notify.js') !!}

        <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
{!! HTML::script('assets/js/paper-dashboard.js') !!}

        <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
{!! HTML::script('assets/js/demo.js') !!}

{!! HTML::script('assets/js/jquery.canvasjs.min.js') !!}

<script type="text/javascript">
    $(document).ready(function(){

        demo.initChartist();

        $.notify({
            icon: 'ti-gift',
            message: "<b>3rdbell Analytics Dashboard</b>"

        },{
            type: 'success',
            timer: 500
        });

    });
</script>

<script>

    $(document).ready(function(){

        var pieArray = <?php echo json_encode($pieData); ?>;
        //console.log(pieArray.musicVideos);
        var myColor = ["#39ca74","#e54d42","#f0c330","#3999d8"];
        var myData = [pieArray.dramaVideos,pieArray.musicVideos,pieArray.movieVideos,pieArray.lifestyleVideos];
        //var myData = [80,23,15,7];
        var myLabel = ["H","H","H","W"];

        function getTotal(){
            var myTotal = 0;
            for (var j = 0; j < myData.length; j++) {
                myTotal += (typeof myData[j] == 'number') ? myData[j] : 0;
            }
            return myTotal;
        }

        function plotData() {
            var canvas;
            var ctx;
            var lastend = 0;
            var myTotal = getTotal();
            var doc;
            canvas = document.getElementById("canvas");
            var x = (canvas.width)/2;
            var y = (canvas.height)/2;
            var r = 100;

            ctx = canvas.getContext("2d");
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            for (var i = 0; i < myData.length; i++) {
                ctx.fillStyle = myColor[i];
                ctx.beginPath();
                ctx.moveTo(x,y);
                ctx.arc(x,y,r,lastend,lastend+(Math.PI*2*(myData[i]/myTotal)),false);
                ctx.lineTo(x,y);
                ctx.fill();

                // Now the pointers
                ctx.beginPath();
                var start = [];
                var end = [];
                var last = 0;
                var flip = 0;
                var textOffset = 0;
                var precentage = (myData[i]/myTotal)*100;
                start = getPoint(x,y,r-20,(lastend+(Math.PI*2*(myData[i]/myTotal))/2));
                end = getPoint(x,y,r+20,(lastend+(Math.PI*2*(myData[i]/myTotal))/2));
                if(start[0] <= x)
                {
                    flip = -1;
                    textOffset = -50;
                }
                else
                {
                    flip = 1;
                    textOffset = 10;
                }
                ctx.moveTo(start[0],start[1]);
                ctx.lineTo(end[0],end[1]);
                ctx.lineTo(end[0]+10*flip,end[1]);
                ctx.strokeStyle = "#bdc3c7";
                ctx.lineWidth   = 2;
                ctx.stroke();
                // The labels
                ctx.font="15px Arial";
                ctx.fillText(precentage.toFixed(2)+"%",end[0]+textOffset,end[1]-4);
                // Increment Loop
                lastend += Math.PI*2*(myData[i]/myTotal);

            }
        }
        // Find that magical point
        function getPoint(c1,c2,radius,angle) {
            return [c1+Math.cos(angle)*radius,c2+Math.sin(angle)*radius];
        }
        // The drawing
        plotData();

    })
</script>

<script>

    function viewGraph(){
        $('.graphics').show();
        $('.column').css('height','0');
        sum=0;
        $('table tr').each(function(index) {
             sum += parseInt($(this).children('td').eq(1).text());
        });
        $('table tr').each(function(index) {
            var ha = $(this).children('td').eq(1).text();
            console.log(ha);
            $('#col'+index).animate({height:ha+'%'}, 1500).html("<div>"+ha+"</div>");
        });
    }

</script>

<script type="text/javascript">

    window.onload = function() {

        var dataPoints = [];
        var chart;
        var url = '{{ URL::route('live.user.feed') }}';

        $.ajax({
            url: url,
            type: "GET",
            success: function(response) {

                $.each(response.liveData,function(key,value){
                    //console.log(response);
                    dataPoints.push({
                        x: new Date(value.updated_at),
                        y: value.user_count
                    });
                });
                chart = new CanvasJS.Chart("chartContainer",{
                    title:{
                        text:""
                    },
                    data: [{
                        type: "area",
                        dataPoints : dataPoints,
                    }],
                    animationEnabled: true,
                    //theme:"theme2"
                    backgroundColor:"#B0E0E6",
                    zoomEnabled:true
                });
                chart.render();
                updateChart();

            },
            error: function(xhr, textStatus, thrownError) {

            }
        });

        function updateChart() {

            var url = '{{ URL::route('live.user.feed.latest') }}';
            $.ajax({

                url: url,
                type: "GET",
                success: function(response) {
                    console.log(response.latestData);
                    dataPoints.push({
                           x: new Date(response.latestData.updated_at),
                           y: response.latestData.user_count,
                        });
                    chart.render();
                    setTimeout(function(){
                        updateChart()
                    }, 1000);

                },
                error: function(xhr, textStatus, thrownError) {

                }
            });
        }
    }
</script>

