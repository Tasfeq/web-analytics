<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>3rdbell Analytics Panel</title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />

<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>

<!-- Bootstrap core CSS     -->
{!! HTML::style('assets/css/bootstrap.min.css') !!}
        <!-- Animation library for notifications   -->
{!! HTML::style('assets/css/animate.min.css') !!}
        <!--  Paper Dashboard core CSS    -->
{!! HTML::style('assets/css/paper-dashboard.css') !!}
        <!--  CSS for Demo Purpose, don't include it in your project     -->
{!! HTML::style('assets/css/demo.css') !!}
{!! HTML::style('assets/css/themify-icons.css') !!}

        <!-- Css for charts -->
{!! HTML::style('assets/css/customchart.css') !!}

{!! HTML::style('assets/css/styleLogin.css') !!}
