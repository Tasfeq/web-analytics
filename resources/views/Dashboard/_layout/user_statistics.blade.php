@extends('Dashboard._layout.master')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h5 class="title">User Statistics</h5>
                        </div>
                        <div class="content">

                            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Age</th>
                                    <th>Account Created</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $key=>$user)
                                <tr class="user-data" data-id="{{$user->id}}" style="cursor: pointer">
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>01717734389</td>
                                    <td>61</td>
                                    <td>{{$user->created_at}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">User Specific Chart</h4>
                            <h4 id="user-name"></h4>
                        </div>
                        <div class="content">
                            <div id="contenitore" tabindex='1' style="display: none">
                                <div class="left">
                                    <table>
                                        <caption>Used Browser List (%)</caption>
                                        <tbody>
                                        <tr><td>Chrome</td><td id="chrome"></td><td style="background-color:#336699">&nbsp;</td></tr>
                                        <tr><td>Opera</td><td id="opera"></td><td style="background-color:#003366">&nbsp;</td></tr>
                                        <tr><td>IE</td><td id="ie"></td><td style="background-color:#ff6600">&nbsp;</td></tr>
                                        <tr><td>Mozilla</td><td id="mozilla"></td><td style="background-color:#ffcc00">&nbsp;</td></tr>
                                        <tr><td>Safari</td><td id="safari"></td><td style="background-color:#7CFC00">&nbsp;</td></tr>
                                        </tbody></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Total Content Viewed</h4>
                        </div>
                        <div class="content timer" style="display: none">
                            <div class="icon-big icon-info text-center">
                                <i class="ti-time"></i> <span class="Count" id="time"> 300</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Mostly Used Platform</h4>
                        </div>
                        <div class="content platform" style="display: none">
                            <div class="icon-big icon-info text-center">
                                <i id="platform-icon" class=""></i> <span class="platform" id="platform"> Desktop</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop


@section('page-specific-js')

    {!! HTML::script('assets/js/jquery-1.10.2.js') !!}
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            $('#example').dataTable({
                "order": [],
                "oLanguage": {
                    "sInfo": "Showing _START_ to _END_ of _TOTAL_ Users",
                    "sInfoEmpty": "Showing 0 to 0 of 0 Users",
                    "sEmptyTable": "No information available"
                }
            });

            $('#example').on('click','.user-data',function(){

                userId=$(this).data('id');
                name=$(this).find("td:first").text();

                var url = '{{ URL::route('search.result') }}';
                $.ajax({
                    url: url,
                    data: { 'user-id' :userId } ,
                    type: "GET",
                    success: function(response) {
                        setData(response,name);
                    },
                    error: function(xhr, textStatus, thrownError) {
                        alert('Something went to wrong.Please Try again later...');
                    }
                });
            })

            function setData(response,name){

                console.log(response);
                $('#contenitore').show();
                var formhtml = '';
                formhtml = '<h4>'+name+'</h4>';
                document.getElementById('user-name').innerHTML = name;
                $('#contenitore').focus();

                document.getElementById('chrome').innerHTML = response.browserDataInPercent.chrome;
                document.getElementById('opera').innerHTML = response.browserDataInPercent.opera;
                document.getElementById('ie').innerHTML = response.browserDataInPercent.explorer;
                document.getElementById('mozilla').innerHTML = response.browserDataInPercent.mozilla;
                document.getElementById('safari').innerHTML = response.browserDataInPercent.safari;

                showTime(response.totalHours);
                showPlatform(response.platformInfo);
            }

            function showTime(time){
                document.getElementById('time').innerHTML =time;
                $('.content .timer').show();
                $('.Count').each(function () {
                    var $this = $(this);
                    jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
                        duration: 3000,
                        easing: 'swing',
                    });
                });
            }

            function showPlatform(platform){

                $('.content .platform').show();
                $('#platform-icon').removeClass().addClass('ti-'+platform.res);
                document.getElementById('platform').innerHTML =platform.res;
            }

        })
    </script>

@stop